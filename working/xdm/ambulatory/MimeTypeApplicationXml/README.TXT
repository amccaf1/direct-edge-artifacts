
README.TXT

This XDM package was assembled by NIST to support Meaningful Use Stage 3 testing.
For further information see the Google Group 
       transport-testing-tool@googlegroups.com

The content of this package:

README.TXT                  - this file
INDEX.HTM                   - simple web interface to data
IHE_XDM/                    - content holder
   SUBSET01/                - metadata/documents package 1
      ToC_Ambulatory.xml    - CCDA document
      METADATA.XML          - XDM metadata for CCDA
